const Twitter = require('twitter')
const { whatnow } = require('./helpers')

const client = new Twitter({
  consumer_key:        process.env.CONSUMER_KEY,
  consumer_secret:     process.env.CONSUMER_SECRET,
  access_token_key:    process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
})

const withHashtags = (status, hashtags) => {
  const strHashtags = [
    '#bullshit',
    ...hashtags
  ].join('\n')
  return `${status}\n\n${strHashtags}`
}

const ALPHABET = '#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

client.get('trends/place', {
  id: 23424977 // United States
  // id: 2487956 // San Francisco
}, (error, data) => {
  if (error) {
    throw error
  }

  const [h1, h2, h3] = data[0]
    .trends
    .filter(({ name: [hash] }) => hash === '#')
    .filter(({ name }) => {
      return name.split('')
        .map((letter) => ALPHABET.includes(letter))
        .reduce((acc, b) => acc && b, true)
    })
    .map(({ name }) => name)
    .sort(() => [-1, 1][Math.floor(Math.random() * 2)])

  const status = withHashtags(whatnow(), [h1, h2, h3])
  console.log(status)

  client.post('statuses/update', { status }, (error, tweet, response) => {
    if (error) {
      throw error
    }

    console.log(tweet)
  })
})
