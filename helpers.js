const number = (max) => Math.floor(Math.random() * max)
const random = (arr) => arr[number(arr.length)]
const maybe = (thing) => random([true, false]) ? thing : null

const verb         = () => random(require('./sets/verbs.json'))
const adverb       = () => random(require('./sets/adverbs.json'))
const tool         = () => random(require('./sets/tools.json'))
const adjective    = () => random(require('./sets/adjectives.json'))
const preposition  = () => random(require('./sets/prepositions.json'))
const interjection = () => random(require('./sets/interjections.json'))
const bodyPart     = () => random(require('./sets/body-parts.json'))
const pronoun      = () => random(require('./sets/pronouns.json'))
const conjonction  = () => random(require('./sets/conjonctions.json'))
const emoji        = () => random(require('./sets/emojis.json'))
const moment       = () => random(require('./sets/moments.json'))
const girl         = () => capitalize(random(require('./sets/girls.json')))
const boy          = () => capitalize(random(require('./sets/boys.json')))
const room         = () => random(require('./sets/rooms.json'))

const sentence = (parts) => capitalize(parts.join(' '))
const nl = () => '\n'
const paragraph = (sentences) => sentences.join(nl())

const withCast = (cb) => cb(girl(), boy()).join(' ')

const line = (p, sentences) => {
  const [ primarySentence, ...secondarySentences ] = sentences
  return [
    '-',
    primarySentence,
    ',',
    'said',
    p,
    adverb(),
    '.',
    secondarySentences.join(' ')
  ].join(' ')
}

const capitalize = (str) => {
  if (!str) {
    return ''
  }
  const [u, ...l] = Array.from(str)
  return u.toUpperCase() + l.join('')
}

const whatnow = () => random([
  [
    sentence([
      interjection(),
      '!',
      capitalize(tool()),
      'and',
      tool(),
      'for the win'
    ])
  ],

  [
    sentence([
      'I like a good',
      adjective(),
      tool(),
      maybe(emoji())
    ])
  ],

  [
    sentence([
      'true',
      tool(),
      'here'
    ])
  ],

  [
    sentence([
      verb(),
      'like you mean it',
      maybe(emoji())
    ])
  ],

  [
    '1. ',
    sentence([verb(), tool()]),
    nl(),
    '2. ',
    sentence([verb(), tool()]),
    nl(),
    '3. ',
    sentence([verb(), tool()]),
  ],

  [
    sentence([
      adverb(),
      'yes... but',
      adverb()
    ])
  ],

  [
    sentence([
      'I',
      verb(),
      'the',
      tool(),
      'a lot lately',
      maybe(emoji())
    ])
  ],

  [
    sentence([
      'I never learned how to',
      verb(),
      tool()
    ]),
    nl(),
    sentence([
      'but',
      interjection(),
      '...'
    ])
  ],

  [
    sentence([
      adjective(),
      moment()
    ])
  ],

  [
    sentence([
      random([girl(), boy()]),
      'and',
      random([girl(), boy()]),
      'are my new favorite names'
    ])
  ],

  [
    sentence([
      'From now on you can call me',
      random([girl(), boy()])
    ])
  ],

  [
    withCast((p1, p2) => [
      paragraph([
        '[SCENE]',
        sentence([ 'the', moment(), 'is', adjective() ]),
        sentence([ p1, "and", p2, 'are in the', room() ]),
        
        '[DIALOG]',
        line(p2, [
          sentence([ 'I love to', verb(), 'your', bodyPart(), 'with my', bodyPart() ])
        ]),
        line(p1, [
          sentence([ interjection(), p2 ]),
          sentence([ `only you can`, adverb(), verb(), 'with your', bodyPart() ])
        ])
      ])
    ])
  ]
]
  .map((e)    => e.filter(ee => Boolean(ee)))
  .map((e)    => e.join(''))
)


module.exports = { whatnow }
